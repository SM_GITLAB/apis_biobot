from django.shortcuts import render
from accounts import serializers
from accounts.models import UserProfile
from rest_framework.views  import APIView
from rest_framework import status
from rest_framework.response import Response
from django.template.loader import render_to_string
from .tokens import account_activation_token
from django.utils.html import strip_tags
from django.core.mail import send_mail, EmailMultiAlternatives
from django.http import HttpResponse



# Create your views here.


# activation view

from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.core.mail import EmailMessage
from django.contrib.sites.shortcuts import get_current_site
from django.utils.encoding import force_bytes, force_text

def activate(request, uidb64, token):
    try:
        uid = force_text(urlsafe_base64_decode(uidb64))
        user = UserProfile.objects.get(pk=uid)
    except(TypeError, ValueError, OverflowError, UserProfile.DoesNotExist):
        user = None
    if user is not None and account_activation_token.check_token(user, token):
        user.is_verified = True
        user.save()
        # login(request, user)
        # return redirect('home')
        # return render(request, 'bio_apis/login.html')
        return HttpResponse('Thank you for your email confirmation. Now you can login your account.')
        # return render(request, 'login.html', {'form': form})
    else:
        return HttpResponse('Activation link is invalid!')




class ProviderRegistration(APIView):
    # permission_classes = (AllowAny,)
    # renderer_classes = (JSONRenderer,)
    serializer_class = serializers.UserRegistrationSerializer
    queryset = UserProfile.objects.all()
    # authentication_classes = (TokenAuthentication,)

 
    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)        
        serializer.is_active = False
        created_object = serializer.save()
        current_site = get_current_site(request)
        html_content = render_to_string('emails/email_signup.html', {
        'user':created_object.name,
        'domain':current_site.domain,
        'uid': urlsafe_base64_encode(force_bytes(created_object.pk)),
        'token': account_activation_token.make_token(created_object),
        'mail': created_object.email
        })
        text_content = strip_tags(html_content)
        email = EmailMultiAlternatives(
            #subject
            'Verify your email',
            #content
            text_content,
            #from 
            'BioBot <noreply@thebiobot.com>',
            # to
            [created_object.email,]
        )
        email.attach_alternative(html_content,'text/html')
        email.send()
        return Response(serializer.data, status=status.HTTP_201_CREATED)


        # admin Notification

        html_content_admin = render_to_string('emails/admin_user.html', {
        'user':created_object.name,
        'domain':current_site.domain,
        'uid': urlsafe_base64_encode(force_bytes(created_object.pk)),
        'token': account_activation_token.make_token(created_object),
        'provider_type': created_object.provider_type
        })
        text_content_admin = strip_tags(html_content_admin)
        email_admin = EmailMultiAlternatives(
            #subject
            'Verify your email',
            #content
            text_content_admin,
            #from 
            'BioBot <noreply@thebiobot.com>',
            # to
            ['samidalashankar@gmail.com',]
        )
        email_admin.attach_alternative(html_content_admin,'text/html')
        email_admin.send()
        messages.success(request,'Registered Scuccesfully')
        # return render(request, 'bio_apis/success.html') 
        return Response(serializer.data, status=status.HTTP_201_CREATED)
 
class UserLogin(APIView):
    # permission_classes = (AllowAny,)
    # renderer_classes = (JSONRenderer,)
    serializer_class = serializers.UserLoginSerializer
    # authentication_classes = (TokenAuthentication,)
 
    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        return Response(serializer.data, status=status.HTTP_200_OK)
 