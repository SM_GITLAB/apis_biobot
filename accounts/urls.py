from django.urls import path, include
from rest_framework.routers import DefaultRouter
from accounts import views
from django.conf.urls import url




urlpatterns = [  
    path('signup/',views.ProviderRegistration.as_view(),name='Sign UP'),
    path('login/',views.UserLogin.as_view(),name='Login'),
    url(r'^activate/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        views.activate, name='activate'),
    
]
