from django.db import models
from django.utils.crypto import get_random_string
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin, BaseUserManager,User
from datetime import datetime
from django.conf import settings
from django.dispatch import receiver
from django.db.models.signals import post_save
import uuid


 
# Create your models here.

def UniqueId():
    from django.utils.crypto import get_random_string
    return ''.join(get_random_string(length=6))

PROVIDER_TYPE_CHOICES = (
      (1, 'Institution'),
      (2, 'Private Practice'),
      (3, 'Individual Provider'),
) 

USER_TYPE_CHOICES = (
      (1, 'App Support'),
      (2, 'Provider Admin'),
      (3, 'Provider'),
      (4, 'Anonymous'),

)

class UserProfileManager(BaseUserManager):

    def create_user(self,email,name,password=None):
        if not email:
            raise ValueError('email is required')
        email =self.normalize_email(email)
        user = self.model(email=email,name=name)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, name,password):

        user=self.create_user(email,name, password)
        user.is_superuser=True    
        user.is_staff = True
        user.save(using=self._db)
        return user


    def create_provider(self, email,name,provider_description,provider_type, password=None):
        if not email:
            raise ValueError('email is required')
        email =self.normalize_email(email)
        user = self.model(provider_type=provider_type, provider_description=provider_description, 
                          email=self.normalize_email(email),name=name)

        user.set_password(password)
        user.save(using=self._db)
        return user

class UserProfile(AbstractBaseUser,PermissionsMixin):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    email = models.EmailField(db_index=True, unique=True)
    name = models.CharField(max_length=100)
    is_staff=models.BooleanField(default=False)
    is_active=models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    provider_type = models.CharField(max_length=50,null=True)
    user_id = models.CharField(max_length=50,default=UniqueId,unique=True)
    user_type = models.IntegerField(choices=USER_TYPE_CHOICES,null=True,default=4)
    provider_description = models.TextField(blank=True,null=True)
    is_verified=models.BooleanField(default=False)

    objects=UserProfileManager()

    USERNAME_FIELD='email'
    REQUIRED_FIELDS = ['name',]

    def get_full_name(self):
        return self.name

    def get_short_name(self):
        return self.name

    def __str__(self):
       return self.email  

from rest_framework.authtoken.models import Token

@receiver(post_save,sender = 'accounts.UserProfile')
def token(sender,instance=None,created=False,**kwargs):
    if created:
        Token.objects.create(user=instance) 
