## user profile serializer

from rest_framework import serializers
from rest_framework import serializers, exceptions
from accounts.models import UserProfile,Token
from django.contrib.auth import authenticate
from django.core.exceptions import ValidationError



class UserRegistrationSerializer(serializers.ModelSerializer):
    password = serializers.CharField(
        max_length=128,
        min_length=6,
        style = {'input_type':'password'}  ,
        write_only=True
        
    )
    user_id = serializers.CharField(
        max_length=128,
        min_length=6,
        read_only=True
    )

    
    class Meta:
        model = UserProfile
        fields = ('id','email','name','password','user_id','provider_type','provider_description')
 
    def create(self, validated_data):
        return UserProfile.objects.create_provider(**validated_data)


from django.contrib.auth import authenticate
class UserLoginSerializer(serializers.Serializer):
    email = serializers.CharField(max_length=255)
    password = serializers.CharField(max_length=128, write_only=True)
    token = serializers.CharField(max_length=255, read_only=True)
    user_id = serializers.CharField(max_length=255, read_only=True)
    provider_type = serializers.CharField(max_length=255, read_only=True)
    user_type = serializers.CharField(max_length=255, read_only=True)


    def validate(self, data):

        email = data.get('email', None)
        password = data.get('password', None)
 
        user = authenticate(username=email, password=password)

        if user is None:
            raise serializers.ValidationError(
                'A user with this email and password is not found.'
            )
        try:
            userObj = UserProfile.objects.get(email=user.email)
        except UserProfile.DoesNotExist:
            userObj = None 
 
        try:
            if userObj is None:
                userObj = Employee.objects.get(email=user.email,)
        except Employee.DoesNotExist:
            raise serializers.ValidationError(
                'User with given email and password does not exists'
            )        
        if not userObj.is_verified:
            raise serializers.ValidationError(
                'This user has been deactivated.'
            )   
      
        return {
            'user_id': userObj.id,
            'email': userObj.email,
            'provider_type': userObj.provider_type,
            'user_type':userObj.user_type,
            'token': Token.objects.get(user=userObj).key
            
        }
