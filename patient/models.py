from django.db import models
from accounts.models import UserProfile
from datetime import datetime
import uuid
# Create your models here.

def UniqueId():
    from django.utils.crypto import get_random_string
    return ''.join(get_random_string(length=6))

    
   


class PatientDetails(models.Model):
    created_by = models.ForeignKey(UserProfile,on_delete=models.CASCADE)
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False) 
    first_name = models.CharField(max_length=250,default=0)
    last_name = models.CharField(max_length=250,default=0)
    age  = models.IntegerField(default=0)
    race = models.CharField(max_length=250,default=0)
    gender = models.CharField(max_length=250,default=0)
    smoking = models.CharField(max_length=250,default=0)
    marital_status = models.CharField(max_length=250,default=0)
    insurance = models.CharField(max_length=250,default=0)
    zipcode = models.CharField(max_length=250,default=0)
    bmi = models.IntegerField(default=0)
    height_feet = models.IntegerField(default=0)
    height_inch = models.IntegerField(default=0)
    weight = models.IntegerField(default=0)
    mrn  = models.IntegerField(default=0)
    start_date = models.DateTimeField(null=True, blank=True)
    end_date = models.DateTimeField(null=True, blank=True)
    
    
    def __str__(self):
       return f"{self.first_name} {self.last_name}"

    @property
    def medicals(self):
        return self.medical_set.all() 
    



class Medical(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False) 
    patient_profile = models.ForeignKey('patient.PatientDetails', on_delete=models.CASCADE)
    procedure = models.CharField(max_length=250,default=0)
    comorbidities = models.CharField(max_length=250,default=0)
    living_situation = models.CharField(max_length=250,default=0)
    asa_status = models.CharField(max_length=250,default=0)
    opioid_medications  = models.CharField(default=2,max_length=250)
    surgical_history = models.CharField(max_length=250,default=0)
    narcotics_use = models.CharField(max_length=250,default=0)
    opioid_allergies = models.CharField(max_length=250,default=0)
    diabetes = models.BooleanField(default=False)
    deconditioned = models.BooleanField(default=False)
    history_of_addiction = models.CharField(max_length=250,default=0)
    inpatient = models.BooleanField(default=False)
    statirs_at_home = models.BooleanField(default=False)
    dob = models.DateTimeField(auto_now_add=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)


    def __str__(self):
        return self.patient_profile


    def __str__(self):
       return f"{self.patient_profile}"


