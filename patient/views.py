from django.shortcuts import render
from patient.serializers import PatientProfileSerializer,PatientSearchSerializer,PatientMedicalSerializer,DetailsSerializer
from patient.models import  Medical,PatientDetails
from accounts.models import UserProfile
from rest_framework.generics import get_object_or_404
from rest_framework import mixins,generics
from rest_framework.authentication import TokenAuthentication
from rest_framework import permissions,viewsets
from rest_framework.response import Response

# Create your views here.





class PatientSearchAPIView(generics.ListAPIView):
    permission_classes = [permissions.IsAuthenticated]
    authentication_classes = (TokenAuthentication,)
    queryset = PatientDetails.objects.all()
    serializer_class = PatientSearchSerializer
    

    def get_queryset(self):
        user = self.request.user
        return PatientDetails.objects.filter(created_by=self.request.user)



class PatientFullAPIView(viewsets.ModelViewSet):
    serializer_class = DetailsSerializer
    queryset = PatientDetails.objects.all()
    lookup_field = 'id'

    def get_queryset(self):
        user = self.request.user
        return PatientDetails.objects.filter(created_by=self.request.user.id)        