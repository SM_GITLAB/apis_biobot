from django.urls import path, include
# from patient.views import PatientProfileAPIView,PatientFullAPIView,PatientMedicalDetailAPIView,PatientProfileDetailAPIView,PatientSearchAPIView,PatientDetailView,PatientMedicalDetailAPIView2
from rest_framework.routers import DefaultRouter
from patient.views import PatientFullAPIView,PatientSearchAPIView

router = DefaultRouter()

router.register('patient-details', PatientFullAPIView, basename='patient-details')





urlpatterns = [
    # path('patient/',PatientProfileAPIView.as_view(),name='patient'),
    # path('patient/<uuid:pk>/',PatientProfileDetailAPIView.as_view(),name='patient detail'),
    path('patientsearch/', PatientSearchAPIView.as_view(), name='PatientSearch'),
    path('', include(router.urls))

]