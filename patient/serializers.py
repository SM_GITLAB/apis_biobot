from rest_framework import serializers, exceptions
from accounts.models import UserProfile
from patient.models import Medical,PatientDetails
from django.contrib.auth import authenticate



class PatientProfileSerializer(serializers.ModelSerializer):
    # created_by =serializers.UUIDField(source = 'created_by.id',read_only=True)
    # medicals = PatientMedicalSerializer(many=False)   

    class Meta:
        model = PatientDetails
        fields =[
            "id",
            "created_by",
        ]

        extra_kwargs = {
            'id':{'read_only':True},
            'UniqueId':{'read_only':True},
            # 'created_by':{'read_only':True},
            'modified_by':{'read_only':True},
        }
        



class PatientMedicalSerializer(serializers.ModelSerializer):
    class Meta:
        model = Medical
        fields = "__all__"

        extra_kwargs = {
            'id':{'read_only':True},
            'UniqueId':{'read_only':True},

        }        



class PatientSearchSerializer(serializers.ModelSerializer):
   
    class Meta:
        model = PatientDetails
        fields = ['id','first_name','last_name','mrn','age']
        # fields = "__all__"

        extra_kwargs={
            'id':{
                'read_only': True               
            },
            'mrn':{
                'read_only': True               
            }
        }
        

## new Patient Serializers


class MedicalSerializer(serializers.ModelSerializer):
    id = serializers.UUIDField(required=False)

    class Meta:
        model = Medical
        fields = [
            'id',
            'patient_profile',            
            'procedure',"comorbidities","asa_status",
            "opioid_medications","opioid_allergies","living_situation","diabetes",
            "surgical_history",'narcotics_use',"deconditioned",
            "history_of_addiction","inpatient","statirs_at_home",
            "dob",

        ]
        read_only_fields = ('patient_profile',)        


class DetailsSerializer(serializers.ModelSerializer):
    medicals = MedicalSerializer(many=True)
    # created_by =serializers.UUIDField(source = 'created_by')    

    class Meta:
        model = PatientDetails
        fields = ["id","created_by","first_name",
                    "last_name","age","race","gender",     
                    "smoking","marital_status","insurance","zipcode",
                    "bmi","height_feet","height_inch","weight","mrn","medicals",]

    def create(self, validated_data):
        medicals = validated_data.pop('medicals')  
        patient_profile = PatientDetails.objects.create(**validated_data)        
        for choice in medicals:
            Medical.objects.create(**choice, patient_profile=patient_profile)
        return patient_profile
    


    def update(self, instance, validated_data):
        medicals = validated_data.pop('medicals')
        instance.first_name = validated_data.get("first_name", instance.first_name)
        instance.created_by = validated_data.get("created_by", instance.created_by)
        instance.last_name = validated_data.get("last_name", instance.last_name)
        instance.age = validated_data.get("age", instance.age)
        instance.race = validated_data.get("race", instance.race)
        instance.gender = validated_data.get("gender", instance.gender)
        instance.smoking = validated_data.get("smoking", instance.smoking)
        instance.marital_status = validated_data.get("marital_status", instance.marital_status)
        instance.insurance = validated_data.get("insurance", instance.insurance)
        instance.zipcode = validated_data.get("zipcode", instance.zipcode)
        instance.weight = validated_data.get("weight", instance.weight)
        instance.height_feet = validated_data.get("height_feet", instance.height_feet)
        instance.height_inch = validated_data.get("height_inch", instance.height_inch)
        instance.bmi = validated_data.get("bmi", instance.bmi)
                
        instance.save()
        keep_choices = []
        for choice in medicals:
            if "id" in choice.keys():
                if Medical.objects.filter(id=choice["id"]).exists():
                    c = Medical.objects.get(id=choice["id"])
                    c.procedure = choice.get('procedure', c.procedure)
                    c.comorbidities = choice.get('comorbidities', c.comorbidities)
                    c.asa_status = choice.get('asa_status', c.asa_status)
                    c.opioid_medications = choice.get('opioid_medications', c.opioid_medications)
                    c.surgical_history = choice.get('surgical_history', c.surgical_history)
                    c.narcotics_use = choice.get('narcotics_use', c.narcotics_use)
                    c.opioid_allergies = choice.get('opioid_allergies', c.opioid_allergies)
                    c.diabetes = choice.get('diabetes', c.diabetes)
                    c.deconditioned = choice.get('deconditioned', c.deconditioned)
                    c.history_of_addiction = choice.get('history_of_addiction', c.history_of_addiction)
                    c.inpatient = choice.get('inpatient', c.inpatient)
                    c.statirs_at_home = choice.get('statirs_at_home', c.statirs_at_home)


                    c.save()
                    keep_choices.append(c.id)
                else:
                    continue
            else:
                c = Medical.objects.create(**choice, patient_profile=instance)
                keep_choices.append(c.id)

        for choice in instance.medicals:
            if choice.id not in keep_choices:
                choice.delete()

        return instance



